const knexModule = require("knex");

const dbConnection = knexModule({
  client: "mssql",
  connection: {
    host: process.env.DB_HOST,
    user: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE,
    port: 1433,
  },
});

module.exports = dbConnection;
