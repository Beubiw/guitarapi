const express = require("express");
const db = require("../../modules/db");

const router = express.Router();

/**
 * Get list of guitar categories
 */
router.get("/", async (req, res) => {
  const category = await db("guitarCategory");
  return res.status(200).json({ category });
});

/**
 * Get a guitar category
 */
router.get("/:id", async (req, res) => {
  const category = await db("guitarCategory")
    .where("id", req.params.id)
    .first();
  if (category) {
    res.status(200).json({ category });
  } else {
    res.status(404).json({ status: "not found" });
  }
  return null;
});

module.exports = router;
