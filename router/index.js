const OpenApiValidator = require("express-openapi-validator");

const { Router } = require("express");

const home = require("./home");
const guitar = require("./guitar");
const guitarCategory = require("./guitarCategory");
const brand = require("./brand");

const router = Router();

/*
router.use(
  OpenApiValidator.middleware({
    apiSpec: "./specs/api.yml",
    validateRequests: true,
    validateResponses: true,
  })
);*/

router.use("/", home);
router.use("/guitar", guitar);
router.use("/guitarCategory", guitarCategory);
router.use("/brand", brand);

module.exports = router;
