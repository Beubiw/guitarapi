const express = require("express");
const db = require("../../modules/db");

const router = express.Router();

//Get brand
router.get("/", async (req, res) => {
  const brand = await db("brand");
  return res.send({ brand });
});

module.exports = router;
