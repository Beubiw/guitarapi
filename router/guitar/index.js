const express = require("express");
const db = require("../../modules/db");

const router = express.Router();

//Get guitar
router.get("/", async (req, res) => {
  const guitar = await db("guitar");
  return res.send({ guitar });
});

module.exports = router;
