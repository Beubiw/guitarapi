const express = require("express");
const db = require("../../modules/db");

const router = express.Router();

//Get guitar
router.get("/", async (req, res) => {
  const pageContent = `
    <h1>GUITAR SHOP API</h1>
    <br><br>
    <h3>GET /guitar</h3>
    <h3>GET /guitar/:id NOT WORKING</h3>
    <h3>GET /guitarCategory</h3>
    <h3>GET /guitarCategory/:id NOT WORKING</h3>
    <h3>GET /brand</h3>
    <h3>GET /brand/:id NOT WORKING</h3>
  `;
  return res.send(pageContent);
});

module.exports = router;
