const express = require("express");
const dotenv = require("dotenv");
const cors = require("cors");

dotenv.config();

const router = require("./router");

const app = express();
const port = 5000;

app.use(express.static("public"));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use(cors());

app.use("/", router);

app.listen(port, () => {
  console.log(`App listening at: http://localhost:${port}`);
});
